package ua.revolution.core.persistence;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.revolution.core.model.User;
import ua.revolution.core.persistence.dao.UserService;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DataSourceTest extends AbstractSpringTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @Test
    public void testDataSource() throws Exception {
        assertNotNull(dataSource);
    }

    @Test
    public void testCreateDeleteUser() throws Exception {
        User user = new User();
        String userName = "userName";
        user.setUserName(userName);

        User persistedUser = userService.getUserByUserName(userName);

        if (persistedUser == null){
            userService.createUser(user);
        }

        persistedUser = userService.getUserByUserName(userName);
        assertNotNull(persistedUser);
        assertEquals(userName, persistedUser.getUserName());

        userService.removeUser(persistedUser);
        persistedUser = userService.getUserByUserName(userName);
        assertNull(persistedUser);
    }
}
