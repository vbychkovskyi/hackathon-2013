package ua.revolution.core.model;

import org.springframework.security.core.GrantedAuthority;

public class Authority implements GrantedAuthority{

    private static final String ROLE_USER = "ROLE_USER";

    @Override
    public String getAuthority() {
        return ROLE_USER;
    }
}
