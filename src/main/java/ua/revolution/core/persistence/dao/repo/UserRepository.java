package ua.revolution.core.persistence.dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.revolution.core.model.User;


public interface UserRepository extends JpaRepository<User, String> {

}
