package ua.revolution.core.persistence.dao;

import ua.revolution.core.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUserByUserName(String userName);
    void createUser(User user);


    void removeUser(User user);
}
