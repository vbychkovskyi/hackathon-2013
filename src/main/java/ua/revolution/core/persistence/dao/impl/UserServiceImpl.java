package ua.revolution.core.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.revolution.core.model.User;
import ua.revolution.core.persistence.dao.UserService;
import ua.revolution.core.persistence.dao.repo.UserRepository;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByUserName(String userName) {
        return userRepository.findOne(userName);
    }

    @Override
    @Transactional
    public void createUser(User user) {
        userRepository.saveAndFlush(user);
    }

    @Override
    public void removeUser(User user) {
        userRepository.delete(user);
    }
}
