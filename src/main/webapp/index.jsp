<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="includes/title.jsp" %>

	<div id="wrapper">
	
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/navigation.jsp" %>
	
		<section id="content">
		<div id="listA">
		<c:forEach var="question" items="${questions}">
		<div class="short-summary">
    			<div class="counts">
					<div class="votes">
						<div class="item-count"><c:out value="${question.votes}"/></div>
						<div>голоси</div>
					</div>
					<div class="status  answered">
						<div class="item-count"><c:out value="${question.answers}"/></div>
						<div>відповіді</div>
					</div>
					<div class="views">
						<div class="item-count"><c:out value="${question.views}"/></div>
						<div>переглядів</div>
					</div>
				</div>
				<div class="question-summary-wrapper">
					<h3><a title="Хочу поставити вінду, не знаю як. Підскажіть!" href="#"><c:out value="${question.headline}"/></a></h3>
					<div class="userinfo">
						<span class="relativetime" ><c:out value="${question.relativetime}"/></span>
						<a href="#"><c:out value="${question.nickname}"/></a>
						<span class="score" title="5 репутації"><span class=""><c:out value="${question.score}"/></span></span>
					</div>
					<div class="tags">
						<a href="#" title="дивитись запитання з міткою 'java'" rel="tag">java</a>
						<a href="#" title="дивитись запитання з міткою 'eclipse'" rel="tag">eclipse</a>
					</div>
				</div>
    	</div>
    	</c:forEach>
    	</div>
		</section>

<%@include file="includes/footer.jsp" %>
		
	</div>
	
</body>
</html>