<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form class="forgot_password">
    <h3>Згадати Все!</h3>
    <div>
        <label>Імя користувача або Email:</label>
        <input type="text" />
        <span class="error">Сталась помилка</span>
    </div>
    <div class="bottom">
        <input type="submit" value="Напомнить пароль"></input>
        <a href="login.jsp" rel="login" class="linkform">Згадали? Перейдіть для Входу</a>
        <a href="register.jsp" rel="register" class="linkform">Ви не зареєстровані? Зареєструватись!</a>
        <div class="clear"></div>
    </div>
</form>