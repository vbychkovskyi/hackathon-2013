<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form class="register">
    <h3>Реєстація</h3>
    <div class="column">
        <div>
            <label>Ім'я:</label>
            <input type="text" />
            <span class="error">Сталась помилка</span>
        </div>
        <div>
            <label>Прізвище:</label>
            <input type="text" />
            <span class="error">Сталась помилка</span>
        </div>
        <div>
            <label>Ваш сайт:</label>
            <input type="text" value="http://"/>
            <span class="error">Сталась помилка</span>
        </div>
    </div>
    <div class="column">
        <div>
            <label>Ім'я користувача(нік):</label>
            <input type="text"/>
            <span class="error">Сталась помилка</span>
        </div>
        <div>
            <label>Email:</label>
            <input type="text" />
            <span class="error">Сталась помилка</span>
        </div>
        <div>
            <label>Пароль:</label>
            <input type="password" />
            <span class="error">Сталась помилка</span>
        </div>
    </div>
    <div class="bottom">
        <input type="submit" value="Регистрация" />
        <a href="login.jsp" rel="login" class="linkform">Вы уже зарегестрированы? Перейдите для Входа</a>
        <div class="clear"></div>
    </div>
</form>