<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form class="login active">
    <h3>Вхід</h3>
    <div>
        <label>Ім'я користувача(нік):</label>
        <input type="text" />
        <span class="error">Сталась помилка</span>
    </div>
    <div>
        <label>Пароль: <a href="forgot_password.jsp" rel="forgot_password" class="forgot linkform">Забули?</a></label>
        <input type="password" />
        <span class="error">Сталась помилка</span>
    </div>
    <div class="bottom">
        <div class="remember"><input type="checkbox" /><span>Запамятати пароль</span></div>
        <input type="submit" value="Войти"></input>
        <a href="register.jsp" rel="register" class="linkform">Ви не зарєстровані? Зареєструватись!</a>
        <div class="clear"></div>
    </div>
</form>