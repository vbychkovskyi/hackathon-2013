<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" href="../../res/html/forms/css/style.css" />
   </head>
    <body>
		<div class="wrapper">
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
                    <jsp:include page="templates/register-form.jsp"/>
                    <jsp:include page="templates/login-form.jsp"/>
                    <jsp:include page="templates/forgotpass-form.jsp"/>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		
<!-- The JavaScript -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript">
			$(function() {
					
				var $form_wrapper	= $('#form_wrapper'),
					
					$currentForm	= $form_wrapper.children('form.active'),
					
					$linkform		= $form_wrapper.find('.linkform');
												
				$form_wrapper.children('form').each(function(i){
					var $theForm	= $(this);
					
					if(!$theForm.hasClass('active'))
						$theForm.hide();
					$theForm.data({
						width	: $theForm.width(),
						height	: $theForm.height()
					});
				});
				
				setWrapperWidth();
				
				$linkform.bind('click',function(e){
					var $link	= $(this);
					var target	= $link.attr('rel');
					$currentForm.fadeOut(400,function(){
						
						$currentForm.removeClass('active');
						
						$currentForm= $form_wrapper.children('form.'+target);
						
						$form_wrapper.stop()
									 .animate({
										width	: $currentForm.data('width') + 'px',
										height	: $currentForm.data('height') + 'px'
									 },500,function(){
										
										$currentForm.addClass('active');
										
										$currentForm.fadeIn(400);
									 });
					});
					e.preventDefault();
				});
				
				function setWrapperWidth(){
					$form_wrapper.css({
						width	: $currentForm.data('width') + 'px',
						height	: $currentForm.data('height') + 'px'
					});
				}
				
				$form_wrapper.find('input[type="submit"]')
							 .click(function(e){
								e.preventDefault();
							 });	
			});
        </script>
    </body>
</html>