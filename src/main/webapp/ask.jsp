<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="includes/title.jsp" %>

	<div id="wrapper">
	
		<%@include file="includes/navigation.jsp" %>
	
		<%@include file="includes/header.jsp" %>
	
		<section id="content">
		<div id="main-body" class="ask-body">
				<div id="askform">
					<form id="fmask" action="${pageContext.request.contextPath}/get_ask" method="POST">
						<div class="form-item">
							<strong><label for="id_title">Заголовок</label></strong> <span class="form-error"></span><br>
							<input autocomplete="off" name="title" maxlength="255" type="text" id="id_title" size="70" tabindex="5"> 
							<div class="title-desc">
								будь ласка, введіть змістовний заголовок для вашого запитання
							</div>
						</div>
						<div id="ask-related-questions" style="height: 0px; display: block;"></div>
						<div class="form-item">
							<div class="resizable-textarea"><span><textarea rows="10" cols="40" name="text" id="editor" class="processed" tabindex="6"></textarea></span></div> 
						</div>
						<div class="form-item">
							<strong><label for="id_tags">Мітки</label></strong><span class="form-error"></span><br>
							<input autocomplete="off" id="id_tags" type="text" name="tags" size="50" class="ac_input" tabindex="7">  
						</div>
						<button type="submit" name="ask" class="submit">Відіслати запитання</button>      
					</form>
				</div>
			</div>
		</section>

<%@include file="includes/footer.jsp" %>
		
	</div>
	
</body>
</html>