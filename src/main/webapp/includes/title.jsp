<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Colonization of Mars</title>
	<meta charset=utf-8>
	<meta name="description" content="COM - це сайт перших колонізаторів Марсу, який дозволяє їм вести блог з новинами і фотографіями з Марсу, а також розміщувати тематичну інформацію."/>
	<meta name="keywords" content="Марс, подорож, відкритий космос, колонізація, озеленення, життя на марсі"/>
	<link rel="icon" href="${pageContext.request.contextPath}/res/img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/res/css/style.css"/>
	<!--<link rel="alternate" type="application/rss+xml" title="Запитання та відповіді - kiqa" href="/feeds/rss">-->
</head>
<body>